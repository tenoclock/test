# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from django.conf import settings
from django.shortcuts import render
from django.views.generic import TemplateView


##################
import nltk
import pickle
from nltk.stem import WordNetLemmatizer
from sklearn.externals import joblib
import numpy as np
##################


class HomePageView(TemplateView):
    def get(self, request, **kwargs):
	file1 = open(os.path.join(settings.PROJECT_ROOT, 'test.txt'),"w")
	#print(os.path.join(settings.PROJECT_ROOT, 'test.txt'))
	file1.write("abc")
	###############################################

	wordnet_lemmatizer = WordNetLemmatizer()
	stopwords = set(w.rstrip() for w in open(os.path.join(settings.PROJECT_ROOT, 'stopwords.txt')))

	pickle_in = open(os.path.join(settings.PROJECT_ROOT, 'wim'),"rb")
	word_index_map = pickle.load(pickle_in)
	#print (word_index_map)
	loaded_model = joblib.load(os.path.join(settings.PROJECT_ROOT, 'finalized_model.sav'))


	live_tokenized = []

	result=0

	live_data = np.zeros((1, (len(word_index_map)+1)))
	def my_tokenizer(s):
    		s = s.lower() # downcase
    		tokens = nltk.tokenize.word_tokenize(s) # string to tokens
    		tokens = [t for t in tokens if len(t) > 2] # remove short words
    		tokens = [wordnet_lemmatizer.lemmatize(t) for t in tokens] # put words into base form
    		tokens = [t for t in tokens if t not in stopwords] # remove stopwords
    		return tokens

	def tokens_to_vector(tokens, label):
	    x = np.zeros(len(word_index_map) + 1) 
	    for t in tokens:
	        i = word_index_map[t]
	        x[i] += 1
	    x = x / x.sum() 
	    x[-1] = label
	    return x

#for review in live_reviews:
	zoken=my_tokenizer("The man is good")
	live_tokenized.append(zoken)

	for tokens in live_tokenized:
	    xd = tokens_to_vector(tokens, 1)
	    live_data[0,:] = xd
	
	Z=live_data[:,:-1]

	print "I think that your review is: "
	m=loaded_model.predict(Z)
	print(m)
#######################################################################
        return render(request, 'index.html', context=None)


class AboutPageView(TemplateView):
    template_name = "about.html"
