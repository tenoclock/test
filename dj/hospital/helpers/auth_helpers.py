from django.contrib.auth import login,authenticate
from django.contrib.auth.models import User
from .. import models
def check_unique_field(field_name, field_value):
    if field_name == "username":
        return not bool(len(User.objects.filter(username=field_value)))
    return not bool(len(User.objects.filter(email=field_value)))

def passwords_match(password, repassword):
    return password == repassword

def login_user(username, password, request):
    user = authenticate(username=username,password=password)
    if user is not None:
        login(request, user)
        return True
    return False

def signup_patient(request):
    first_name = request["first_name"]
    last_name = request["last_name"]
    username = request["uname"]
    password = request["psw"]
    confirm_password = request["psw-repeat"]
    email = request["email"]
    phno = request["phno"]
    try:
        if check_unique_field("username", username) and \
        passwords_match(password, confirm_password) and \
        check_unique_field("email", email):
            user = User.objects.create(username=username,
                                        first_name=first_name,
                                        last_name=last_name,
                                        email=email)
            user.set_password(password)
            user.save()
            patient = models.Patient.objects.create(user = user, phno = phno)
            return True
    except:
        return False

def signup_hospital(request):
    username = request["uname"]
    password = request["psw"]
    confirm_password = request["psw-repeat"]
    email = request["email"]
    phno = request["phno"]
    license = request["license"]
    location = request["location"]
    description = request["description"]
    try:
        if check_unique_field("username", username) and \
        passwords_match(password, confirm_password) and \
        check_unique_field("email", email):
            user = User.objects.create(username=username,
                                        email=email)
            user.set_password(password)
            user.save()
            hospital = models.Hospital.objects.create(user = user,license=license,location=location, description=description, phno = phno)
            return True
    except:
        return False

def is_hospital(user):
    return len(models.Hospital.objects.filter(user = user))!=0
