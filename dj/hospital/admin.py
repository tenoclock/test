from django.contrib import admin
from . import models
# Register your models here.

class PatientAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.Patient, PatientAdmin)
admin.site.register(models.Hospital, PatientAdmin)
