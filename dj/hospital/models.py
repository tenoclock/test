from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Patient(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    phno = models.CharField(max_length=10)
    def __str__(self):
        return str(self.user)


class Hospital(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    license = models.CharField(unique = True, max_length=10)
    phno = models.CharField(max_length=10)
    location = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    def __str__(self):
        return str(self.user)
