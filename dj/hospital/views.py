from django.shortcuts import render, redirect
from django.http import  HttpResponse
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from django.contrib.auth.models import User
from . import models
from .helpers import auth_helpers

@cache_control(no_cache=True,must_revalidate=True,no_store=True)
@login_required(login_url="/hospital")
def home_view(request):
    if auth_helpers.is_hospital(request.user):
        return render(request, "dummy_hospital.html")
    return render(request, "dummy.html")
# Create your views here.

def authentication_manager(request):
    if not request.user.is_anonymous:
        return redirect("/hospital/home")
    if request.method == "GET":
        return render(request, "index.html")
    if request.method == "POST":
        req =  request.POST
        username = req["uname"]
        password = req["psw"]
        if req["meta"] == "login":
            #login logic here

            if auth_helpers.login_user(username, password, request):
                return redirect("/hospital/home")
            else:
                return redirect("/hospital")
        elif req["meta"] == "signup":

            #check if similar user exists
            if auth_helpers.signup_patient(request.POST):
                if auth_helpers.login_user(username, password, request):
                    return redirect("/hospital/home")
            return redirect("/hospital")
        else:
            #standard error page
            pass
    return redirect("/hospital")




def logout_view(request):
    if not request.user.is_anonymous:
        logout(request)
    return redirect("/hospital")


def hospital_auth(request):
    if not request.user.is_anonymous:
        return redirect("/hospital/hospital")
    if request.method == "GET":
        return render(request, "hospital_index.html")
    if request.method == "POST":
        req =  request.POST
        username = req["uname"]
        password = req["psw"]
        if req["meta"] == "login":
            #login logic here

            if auth_helpers.login_user(username, password, request):
                return redirect("/hospital/home")
            else:
                return redirect("/hospital/hospital")
        elif req["meta"] == "signup":

            #check if similar user exists
            if auth_helpers.signup_hospital(request.POST):
                if auth_helpers.login_user(username, password, request):
                    return redirect("/hospital/home")
            return redirect("/hospital/hospital")
        else:
            #standard error page
            pass
    return redirect("/hospital/hospital")

def profile_view(request,username):
    try:
        user=User.objects.get(username=username)
        return render(request,"profile_view.html",{"user":user})

    except:
        return redirect("/hospital")


